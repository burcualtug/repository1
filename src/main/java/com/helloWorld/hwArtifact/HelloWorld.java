package com.helloWorld.hwArtifact;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.time.LocalTime;

@RestController
public class HelloWorld {

    @RequestMapping
    public String helloWorld(){
        String time = "The current time is: " + LocalTime.now().toString();
        return "Hello World! " + time;
    }
}
